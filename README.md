# web2paper

A custom [Processwire](http://processwire.com/) installation dedicated to layout and print books. Initially developed to design the two catalogs for Design Parade festival 

## Quick start guide

1. Clone the repository `# git clone git@gitlab.com:Luuse/web2paper.git`
2. `cd to/the/repo`
3. use the makefile to initiate installation: `# make install usr=youMysqlUserName pw=yourMySqlPassword` where **usr** is your mysql username and **pw** your mysql password. Your sudo password will be asked.
4. Then type `make import-db  usr=youMysqlUserName pw=yourMySqlPassword`. Arguments are sames as third point.
5. Voila. You can now log in using "**admin**" user and "**admin**" password following the admin url defined on last installation screen (default: "/processwire")
6. Run `make init docWidth=int docHeight=int unitW=int unitH=int` where int is respectively the width (in millimeters), height (in millimeters), horizontal grid units, vertical grid units. For example, if you want a page size at 200 x 320 mm and a 20 x 30 grid: `make init docWidth=200 docHeight=320 unitW=20 unitH=30`


## Usage

### browser

Chrome is the only browser able to handle every features.

  

### GUI

1. Shortcuts
  - Press **g** to toggle grid
  - Press **b** to toggle bleeds
  - Press **s** to toggle single/double page
  - Press **p** to zoom in
  - Press **m** to zoom out
  - Press **ctrl + p** to prompt Chrome print dialog box (Then select *Save as pdf* in *Destination* part).

2. Navigation
  - You can jump to each books/parts/pages using the nav bar on the right. (see [Templates](#user-content-templates) part to learn how to use, modify or create templates).

      

### Templates
To learn the basis of Processwire usage, check the [documentation](https://processwire.com/docs/).

#### Use existing templates

When creating a new page, you can chose one of the sample templates (*template_2columns*, *template_simple*, *template_simple_invert* and *template_textOnly_flowing*)

#### create new templates

To create a new template, follow these steps:

1. Create a new php file inside `processwire/site/templates` named `template_nameOfYourTemplate.php`

2. Go to processwire admin and create a new template as in the screenshot:
   ![image](img/createTemplate.png)

3. Select the name of the php template you just created in first point. 

4. In the templates list, click on the new template and add the field you need. To learn how to create new fields, see  the [documentation](https://processwire.com/docs/).

5. Go back to `processwire/site/templates` and edit `get_content.php` to include the new template by adding a new `else if ` condition at the end of the statement:

   ```php
   } else if ($template == 'template_nameOfYourTemplate'){
       include('./template_nameOfYourTemplate.php');
     }
   ```

   if you have specific fields, declare a new variable containing them in the statement, like:

   ```php
     $texteEn = $partie->texte_courant->getLanguageValue('en'); // $partie is the current page, texte_courant the needed field
   ```

6. Then open your php template file (something like `template_nameOfYourTemplate.php`) and paste the basic structure:

   ```php
   <div class="singleContent singleContent<?= $key ?> <?= $partie->id ?>">
     <div class="content content0" id="content<?= $partie->id ?>">
   	<!-- 
       	Put here the content desired on the left page, fore example: 
       	<?= texteEn ?>
       -->
     </div>
     <div class="content1">
   	<!-- 
       	Put here the content desired on the left page, fore example: 
   		<?php foreach ($images as $key => $image): ?>
         		<img src="<?= $image->url ?>" alt="">
       	<?php endforeach; ?>
       -->
      </div>
   </div>
   
   ```

7. You can now give style to this template by editing `processwire/site/templates/style/main.less` and calling the class corresponding to you template (i.e: `.template_nameOfYourTemplate{ color: red; }`)

8. Note: if you want a flowing content, like a texte starting from the left page and continuing on the next one, just add a class named `flowing` to the `.singleContent` element wrapping your double page in your template php file: `<div class="singleContent singleContent<?= $key ?> <?= $partie->id ?> flowing"> `and declare you content variable only on the starting page.

#### Page management

To define the number of pages you need to your article, add the *Page needed* field and fill in the number you need (even number, obviously).  

  

### stylize

This tool use [Less](http://lesscss.org) to stylize pages. Use a compiler (like [less-watch-compiler](https://github.com/jonycheung/deadsimple-less-watch-compiler) or [Less-autocompile](https://github.com/loeck/less-autocompile)) to generate css files.

All less files can be found at `processwire/site/templates/style`.

#### gui.less

Everything concerning user interface: navbar, tools bar...

#### css-region.less

A couple of loops to inject content in specified structure. it uses a js library called `css-regoin-polyfill`. You can have a look following `processwire/site/templates/js/lib/css-region-polyfill.js`

#### fonts.less

Write here your @font-face declarations. It currently uses a cdn dedicated to [Hershey-Noaille](http://hershey-noailles.luuse.io/) font family.

#### variables.less

You will find and put in this file the global variables. It currently store document setup (width, height, bleed...), colors and font related variables.

#### documentSetup.less

All document related css must be writed here.

#### printDouble.less and printSingle.less

Two files dedicated to optimise document for print. It hides gui elements, defines page dimensions, optimize colors...

#### reset.less

A custom reset file to avoid semantic styling.

#### main.less

The working styling file, Here you can build a fucking good looking book. 

  

## Pdf export and pre-press settings

To choose single page or double page export, toggle the boolean variable `$double` from false to true in `processwire/site/templates/inc/variables.php`. Double page export is intended for screen preview only.

  

## Limits & bugs

### Polyfill js library

This lib (`processwire/site/templates/js/lib/css-region-polyfill.js`) used to flow content into a specific structure aims to emulate a depreciated css property.  Its causes few issues like:

- Increased loading time
- Bad placement or erratic break when the page is reloaded with zoom and/or double pages activated. To fix that, you can click on the part/page link in the right nav or reload the page after having removed queries in the url. 

## To do

soon
