usr = root
pw = root
dbname = sample
dbfile = sample
pathRepo = $(shell pwd)
docWidth = 210
docHeight = 197
gridW = 20
gridH = 20

install: processwire create-db files-manip symbolic-links config
uninstall: remove-processwire delete-db

processwire:
	wget https://github.com/processwire/processwire/archive/master.zip
	unzip master.zip
	mv processwire-master/ processwire/
	rm -f master.zip

remove-processwire:
	rm -rf processwire

create-db:
	echo "create database $(dbname)" | mysql -u  $(usr) -p$(pw)

import-db: delete-db create-db
	mysql -u $(usr) -p$(pw) $(dbname) < sql/$(dbfile).sql

export-db:
	mysqldump -u $(usr) -p$(pw) $(dbname) > sql/$(dbfile).sql

delete-db:
	echo "drop database $(dbname)" | mysql -u $(usr) -p$(pw)

show-db:
	echo "show databases" | mysql -u $(usr) -p$(pw)

files-manip:
	mv processwire/site-beginner/ processwire/site/
	sudo chmod -R 777 processwire/site/assets/
	sudo chmod -R 777 processwire/site/modules/
	sudo chmod 777 processwire/site/config.php
	sudo chmod 777 -R pw/files/
	mv processwire/htaccess.txt processwire/.htaccess
	rm -fr processwire/site-*

symbolic-links:
	rm -rf processwire/site/assets/files
	ln -s $(pathRepo)/pw/files/ processwire/site/assets/files
	rm -rf processwire/site/modules
	ln -s $(pathRepo)/pw/modules/ processwire/site/modules
	rm -rf processwire/site/templates
	ln -s $(pathRepo)/pw/templates/ processwire/site/templates
	rm -rf processwire/site/config.php
	cp $(pathRepo)/pw/config.php processwire/site/config.php

config:
	replace "config->chmodDir = '0777'" "config->chmodDir = '0700'" -- processwire/site/config.php
	replace "config->chmodFile = '0666'" "config->chmodFile = '0600'" -- processwire/site/config.php
	replace "config->dbUser = 'root'" "config->dbUser = '$(usr)'" -- processwire/site/config.php
	replace "config->dbPass = 'root'" "config->dbPass = '$(pw)'" -- processwire/site/config.php
	rm processwire/install.php

init:
	sed -i "s/pageW : .*mm/pageW : $(docWidth)mm/" pw/templates/styles/variables.less
	sed -i "s/pageH : .*mm/pageH : $(docHeight)mm/" pw/templates/styles/variables.less
	sed -i "s/@unitW : @pageW \/ .*;/@unitW : @pageW \/ $(unitW);/" pw/templates/styles/variables.less
	sed -i "s/@unitW : @pageW \/ .*;/@unitW : @pageW \/ $(unitW);/" pw/templates/styles/variables.less
	sed -i "s/@unitH : @pageH \/ .*;/@unitH : @pageH \/ $(unitH);/" pw/templates/styles/variables.less
	sed -i "s/unitW = .*;/unitW = $(unitW);/" pw/templates/inc/grid.php
	sed -i "s/unitH = .*;/unitH = $(unitH);/" pw/templates/inc/grid.php
	lessc pw/templates/styles/gui.less pw/templates/styles/gui.css
	lessc pw/templates/styles/printDouble.less pw/templates/styles/printDouble.css
	lessc pw/templates/styles/printSingle.less pw/templates/styles/printSingle.css
