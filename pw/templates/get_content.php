
<?php

  $pagesNeeded = $partie->nbPages;

  include('./pages.php');

  $partie->setOutputFormatting(false);

  $titre = $partie->title;
  $titreEn = $partie->title->getLanguageValue('en');
  if ($partie->texte_courant) {
    $texteFr = $partie->texte_courant->getLanguageValue('default');
    $texteEn = $partie->texte_courant->getLanguageValue('en');
  }
  $images = $partie->images;


  if ($template == 'template_simple' ) {

    include('./template_simple.php');

  } else if ($template == 'template_2columns'){

    $artiste = $partie->artiste;
    $pays = $partie->pays;
    if ($pays) {
      $paysFr = $pays->getLanguageValue('default');
      $paysEn = $pays->getLanguageValue('en');
    }

    $sous_titreFr = $partie->sous_titre->getLanguageValue('default');
    $sous_titreEn = $partie->sous_titre->getLanguageValue('en');
    $bio = $partie->infos;
    if ($bio) {
      $bioFr = $bio->getLanguageValue('default');
      $bioEn = $bio->getLanguageValue('en');
    }

    include('./template_2columns.php');
  } else if ($template == 'template_textOnly_flowing') {

    $notes = $partie->infos;
    include('./template_textOnly_flowing.php');

  } else if ($template == 'template_simple_invert'){
    include('./template_simple_invert.php');
  } else if ($template == 'template_test'){

    $textInteressant = $partie->textInteressant->getLanguageValue('default');
    include('./template_test.php');
  }


?>
