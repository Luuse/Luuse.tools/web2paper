<?php
  function rft($content) {
    include ("rft_fr.php");
    foreach ($regex as $input => $output) {
       $content = preg_replace($input, $output, $content);
    }
    return $content;
  }

  function rft_en($content) {
    include ("rft_en.php");
    foreach ($regex as $input => $output) {
       $content = preg_replace($input, $output, $content);
    }
    return $content;
  }
