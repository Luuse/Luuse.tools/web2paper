<?php
  $unitW = 20;
  $unitH = 30;

?>
<div class="grids">
  <div class="grids grid gridCol">
    <?php
      for ($j=0; $j < $unitW ; $j++) {
        echo '<div class="column"></div>';
      }
    ?>
  </div>
  <div class="grids grid gridRow">
    <?php
      for ($j=0; $j < $unitH; $j++) {
        echo '<div class="row"></div>';
      }
    ?>
  </div>
</div>
