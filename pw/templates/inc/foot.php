    <script src="<?php echo $config->urls->templates ?>js/lib/jquery-3.2.1.min.js"></script>
    <script src="<?php echo $config->urls->templates ?>js/lib/jquery.arctext.js" type="text/javascript"></script>
    <script src="<?php echo $config->urls->templates ?>js/lib/jquery.query-object.js" type="text/javascript"></script>
    <script async src="<?php echo $config->urls->templates ?>js/lib/css-region-polyfill.js" type="text/javascript"></script>

    <!-- <script src="<?php echo $config->urls->templates ?>js/michael-json.js" type="text/javascript"></script>
    <script src="<?php echo $config->urls->templates ?>js/main-michael-json.js" type="text/javascript"></script> -->
    <script src="<?php echo $config->urls->templates ?>js/functions.js" type="text/javascript"></script>
    <script src="<?php echo $config->urls->templates ?>js/main.js" type="text/javascript"></script>
    <!-- <script src="<?php echo $config->urls->templates ?>js/lib/less.min.js" type="text/javascript"></script> -->

  </body>
</html>
