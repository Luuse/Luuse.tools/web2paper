<?php header("Access-Control-Allow-Origin: *"); ?>
<?php ini_set('display_errors', 'On'); # mode debug, off sur serveur !!!! ?>
<?php
  include("./variables.php");
  include("./functions.php");
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, shrink-to-fit=no, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Yah2P</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates ?>img/faviconok.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/css-regions.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/main.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/gui.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/documentSetup.css" media="all">
    <?php if ($double == true) { ?>
      <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/printDouble.css" media="all">
    <?php } else { ?>
      <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/printSingle.css" media="all">
    <?php } ?>

  </head>

  <body class="<?= $book ?> <?= $article ?><?php echo $print == true ? ' print' : ''; ?> <?php echo $double == true ? ' double' : ''; ?>" id="<?= $currentPage ?>">

    <?php include('inc/nav.php'); ?>
