<?php
$queries = $_GET;
 ?>

<ul id="tools">
  <li class="sizer"> zoom (p - m)
    <label for="size" class="sizer" ><input type="range" name="sizer" value="100" min="20" max="250"></label>
  </li>
  <li class="gridBtn clicked">grid (g)</li>
  <li class="bleedBtn clicked">bleed (b)</li>
  <li class="singleBtn">single page (s)</li>
</ul>

<ul id="nav">
  <?php
  $home = $pages->get('template=home');
  foreach ($home->children as $key => $book):
    ?>
    <li><?= $book->title ?></li>
    <ul>
      <?php foreach ($book->children as $key => $partie): ?>
        <li>
          <a href="<?= $partie->url ?>" ><?= $partie->title ?></a>
        </li>
        <ul>
          <?php foreach ($partie->children as $key => $element): ?>
              <li>
                <a href="<?= $element->url ?>" ><?= $element->title ?></a>
              </li>
          <?php endforeach; ?>
        </ul>
      <?php endforeach; ?>
    </ul>
   <?php endforeach; ?>
</ul>
