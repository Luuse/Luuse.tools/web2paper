<?php
  include("./inc/head.php");
  $partie = $page;
?>

<section class="part" id="part<?= $partie->id ?>">

  <?php

    $partie->setOutputFormatting(false);

    $artiste = $partie->artiste;
    $paysFr = $partie->pays->getLanguageValue('default');
    $paysEn = $partie->pays->getLanguageValue('en');
    $titre = $partie->title;
    $sous_titreFr = $partie->sous_titre->getLanguageValue('default');
    $sous_titreEn = $partie->sous_titre->getLanguageValue('en');
    $texteFr = $partie->texte_courant->getLanguageValue('default');
    $texteEn = $partie->texte_courant->getLanguageValue('en');
    $bioFr = $partie->infos->getLanguageValue('default');
    $bioEn = $partie->infos->getLanguageValue('en');
    $images = $partie->images;

  ?>

  <?php include('./pages.php'); ?>
  <?php if ($currentPage == 'concours' ){
    include('template_2columns.php');
  } elseif ($currentPage == "expositions") {
    include('expositions.php');
  } ?>

</section>
<?php
  include("./inc/foot.php");
?>
