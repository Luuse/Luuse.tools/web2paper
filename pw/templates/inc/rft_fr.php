<?php

    $regex = array(

    	# input		        # output	                              # comment

                                                                # On commence par retirer tout les espaces avant de mettre les espaces fines
    	"/ ;/"	  =>	    ';',	                                  # espace point-virgule		            => point-virgule
      "/« /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement français ouvrant
      "/« /"	  =>	    '«',	                                  # guillement français ouvrant espace  => guillement anglais ouvrant
      "/“ /"	  =>	    '“',	                                  # guillement français ouvrant espace  => guillement anglais ouvrant
    	"/“ /"	  =>	    '“',	                                  # guillement français ouvrant espace  => guillement français ouvrant
      "/ »/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement français fermant
      "/ »/"	  =>	    '»',	                                  # espace guillement français fermant  => guillement anglais fermant
      "/ ”/"	  =>	    '”',	                                  # espace guillement français fermant  => guillement anglais fermant
    	"/ ”/"	  =>	    '”',	                                  # espace guillement français fermant  => guillement français fermant
      "/ :/"	  =>	    ':',	                                  # espace deux points	                => deux points
    	"/ :/"	  =>	    ':',	                                  # espace deux points	                => deux points
      "/ ;/"	  =>	    ':',	                                  # espace deux points	                => deux points
    	"/ ;/"	  =>	    ':',	                                  # espace deux points	                => deux points

      "/ !/"	  =>	    '!',	                                  # espace point d'exclamation	        => point d'exclamation
    	"/ !/"	  =>	    '!',	                                  # espace point d'exclamation	        => point d'exclamation
      "/ \?/"	  =>	    '?',	                                  # espace point d'interrogation	      => point d'interrogation
    	"/ \?/"	  =>	    '?',	                                  # espace point d'interrogation	      => point d'interrogation
                                                                # On rajoute une balise .thinsp entourant une espace insécable.
                                                                # La gestion de la largeur de l'espace se fait en css avec la propriété letter-spacing
    	// "/;/"	    =>	    '<span class="thinsp">&nbsp;</span>;',  # point virgule  ->bug les caractères spéciaux                     => narrow non breaking space point virgule
      "/;/"	    =>	    '&thinsp;;',	# point-virgule	                        => narrow non breaking space point-virgule
      "/:/"	    =>	    '&thinsp;:',	# deux points	                        => narrow non breaking space deux points
      "/«/"	    =>	    '«&thinsp;',	# guillement ouvrant                  => guillemet ouvrante narrow non breaking space
      "/»/"	    =>	    '&thinsp;»',	# guillement fermant                  => narrow non breaking space  guillemet fermante
    	"/!/"	    =>	    '&thinsp;!',	# point d'exclamation                 => narrow non breaking space point d'exclamation
    	"/\?/"	  =>	    '&thinsp;?',	# point d'interrogation               => narrow non breaking space  guillemet fermante
      // "/.&thinsp;\?/"	  =>	    '.?',
                                                                # Caractères spéciaux
    	"/& /"	  =>	    '&amp; ',	                              # &			                             => &amp;
    	"/\.\.\./"=>	    '&#8230;',	                            # AE			                           => Æ

      # Espace insécable après les mots de 1 et 2 lettre pour le drapeau
      "/ (\b\p{L}{1,2}\b) /u"	      =>	    ' $1&nbsp;',        # cherche tout les mots de 1 et 2 lettre, accentués compris
      "/&nbsp;(\b[a-z]{1,2}\b) /i"	=>	    '&nbsp;$1&nbsp;',    # dans le cas où 2 mots de 1 ou 2 lettres se suivent

      # Espaces insécables Des / les / ces / ses / dans / avec
      "/ des /" => ' des&nbsp;',
      "/ les /" => ' les&nbsp;',
      "/ ces /" => ' ces&nbsp;',
      "/ ses /" => ' ses&nbsp;',
      "/ dans /" => ' dans&nbsp;',
      "/ avec /" => ' avec&nbsp;',

			# Correction des bugs
      '/&amp&thinsp;;/' => '&amp;', # http:
    	'/http&thinsp;:/' => 'http:', # http:
      '/https&thinsp;:/' => 'https:',	# https:
      '/href="mailto&thinsp;:/' => 'href="mailto:', # mailto:
      '/watch&thinsp;?/' => 'watch', # youtube:

    );

?>
