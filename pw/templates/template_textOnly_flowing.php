<div class="singleContent singleContent<?= $key ?> <?= $partie->id ?> flowing">
  <div class="content content0" id="content<?= $partie->id ?>">
    <div class="header">
      <h1><?= $titre ?></h1>
    </div>
    <div class="textSimple textFr">
      <?= $texteFr ?>
    </div>
    <div class="textSimple textEn">
      <?= $texteEn ?>
    </div>
    <div class="textSimple">
      <div class="notes">
        <?= $notes ?>
      </div>
    </div>
  </div>
  <div class="content1">

    </div>
  </div>
</div>
