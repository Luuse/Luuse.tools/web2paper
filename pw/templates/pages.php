<?php

  if ($pagesNeeded) {
    $max = $pagesNeeded;
  } else {
    $max = 2;
  }

  for ($i=0; $i <= $max - 1; $i++) {
    if ($i % 2 == 0): ?>

      <div class="bleed bleed<?= $i ?> singlePages">
        <div class="page pageLeft" id="page_<?= $i ?>">
          <div class="crop cropTop"></div>
          <div class="crop cropLeft"></div>
          <?php include('./inc/grid.php'); ?>
            <div class="structure structure0">
              <?php if ($i == 0){ ?>
                <div class="headerTo">
                </div>
                <div class="textTo textToFirst">

                </div>
              <?php } else { ?>
                <div class="textTo">

                </div>
              <?php } ?>
            </div>

            <div class="pagination"><?= $pagesNb ?></div>

          <div class="crop cropRight"></div>
          <div class="crop cropBottom"></div>
        </div>
      </div>
      <?php $pagesNb ++; ?>
      <?php endif; ?>
      <?php if ($i % 2 != 0): ?>

        <div class="bleed bleed<?= $i ?> singlePages">
          <div class="page pageRight" id="page_<?= $i ?>">
            <div class="crop cropTop"></div>
            <div class="crop cropLeft"></div>
            <?php include('./inc/grid.php'); ?>
              <div class="structure structure0">
                <div class="textTo">                  </div>
              </div>

            <div class="pagination"><?= $pagesNb ?></div>

            <div class="crop cropRight"></div>
            <div class="crop cropBottom"></div>
          </div>
        </div>
        <?php $pagesNb ++; ?>
    <?php endif; ?>
    <?php
  }
?>
