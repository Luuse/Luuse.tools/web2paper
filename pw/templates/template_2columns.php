<div class="singleContent singleContent<?= $key ?> <?= $partie->id ?>">
  <div class="content content0" id="content<?= $partie->id ?>">
    <div class="header">
      <h1 class="artist"><?= $artiste ?></h1>
      <h1 class="test"><?= $test ?></h1>
      <h2 class="country">
        <span class="fr"> <?= $paysFr ?>  </span>
        <span class="en"><?= $paysEn ?></span>
      </h2>
      <h1 class="title"><?= $titre ?></h1>
      <h2 class="subtitle"><span class="fr"> <?= $sous_titreFr ?>  </span><span class="en"><?= $sous_titreEn ?></span></h2>
    </div>
    <div class="text">
      <div class="columnFr columnFr texte_fr"><?= rft($texteFr) ?></div>
      <div class="columnEn columnEn texte_en"><?= rft_en($texteEn) ?></div>
    </div>
    <div class="infos">
      <div class="bioFr bioFr"><?= $bioFr ?></div>
      <div class="bioEn bioEn"><?= $bioEn ?></div>
    </div>
  </div>

  <div class="content1">
    <div class="images">
      <?php foreach ($images as $image): ?>
          <?php if ($print == true) { ?>
            <img src="<?= $image->url ?>" />
          <?php } elseif ($print == false) { ?>
            <img src="<?= $image->size(400, 0)->url ?>" />
          <?php } ?>
          <div class="legend"><?= $image->description ?></div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
