

$(document).ready(function () {
  var tools = $('#tools');
  var sizer = tools.find('.sizer');
  var gridBtn = tools.find('.gridBtn');
  var bleedBtn = tools.find('.bleedBtn');
  var singleBtn = tools.find('.singleBtn');
  var double = $('.double');
  var page = $('.page');
  var expos = $('#expositions');
  var exposImgs = $('#expositions').find('img');

  zoom(sizer, double);
  toggleGrid(gridBtn);
  toggleBleed(bleedBtn);
  toggleSingle(singleBtn);
  showPageId(page);

  var number = 1 + Math.floor(Math.random() * 10000);
  var el = 'rand'

  changeQuery(el, number);

  checkQueryOnLoad();

  imagesPrint(expos, exposImgs);

  $('.singleContent').each(function(){
    if ($(this).hasClass('flowing')) {
      $(this).parents('section').addClass('flowing');
    }
  })


  var ids = [];

  $('.content0').each(function () {
    var id = $(this).attr('id');
    ids.push(id);
  })
  var i = 0;
  $('.structure0').each(function () {
    $(this).attr('id', ids[i]);
    i++;
  })

  // $('.pageFromLeft').region('flowTo', $('.bleed'));

})

$(window).bind("load", function() {
var expos = $('#expositions');
  var exposImgs = expos.find('img');

  $('#expositions .part a, #concours .part a').click(function(e){
    e.preventDefault();
  })

})

$(document).keypress(function (e) {
  var tools = $('#tools');
  var gridBtn = tools.find('.gridBtn');
  var bleedBtn = tools.find('.bleedBtn');
  var singleBtn = tools.find('.singleBtn');
  var windowW = $(window).width();
  var sizer = tools.find('.sizer').children('[type=range]');
  console.log(e.which);
  if (e.which === 103) { // G
    $('.grids').toggle();
    gridBtn.toggleClass('clicked');
    var el = 'grid';
    changeQuery (el);
  }
  if (e.which === 98) { // B
    bleedBtn.toggleClass('clicked');
    $('.bleed').toggleClass('hideBleed');
    var el = 'bleed';
    changeQuery (el);
  }
  if (e.which === 115) { // S
    singleBtn.toggleClass('clicked');
    $('.bleed').toggleClass('singlePages');
    var el = 'single';
    changeQuery (el);
  }
  if (e.which === 112) { // P
    zoomShortcut(sizer, windowW, 'plus');
  }
  if (e.which === 109) { // M
    zoomShortcut(sizer, windowW, 'moins');
  }

})
