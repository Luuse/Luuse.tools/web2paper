function zoom (sizer, double) {
  var windowW = $(window).width();
  sizer.children('[type=range]').on('input change', function () {
    var value = $(this).val() / 100
    var el = 'zoom'
    changeQuery (el, value)

    $('section').css({
      'transform': 'scale(' + value + ')',
      'width': windowW / value,
    })
  })
}

function toggleGrid (btn) {
  btn.click(function () {
    var url = window.location.href
    btn.toggleClass('clicked')
    $('.grids').toggle()
    var el = 'grid'
    changeQuery (el)
  })
}

function toggleBleed (btn) {
  btn.click(function () {
    var url = window.location.href
    btn.toggleClass('clicked')
    $('.bleed').toggleClass('hideBleed')
    var el = 'bleed'
    changeQuery (el)
  })
}

function toggleSingle (btn) {
  btn.click(function () {
    btn.toggleClass('clicked')
    $('.bleed').toggleClass('singlePages')
    var el = 'single'
    changeQuery (el)
  })
}

function changeQuery (el, value) {
  var query = $.query.get(el);
  if (value) {
    query = value
  } else {
    if (query === 'off') {
      query = 'on'
    } else {
      query = 'off'
    }
  }
  var newurl = $.query.SET(el, query).toString();
  window.history.pushState({path:newurl},'',newurl);
}

function checkQueryOnLoad(windowW){
  var windowW = $(window).width()
  var queries = ['single', 'bleed', 'grid', 'zoom']
  for (var i = 0; i < queries.length; i++) {
    var query = $.query.get(queries[i]);
    if (queries[i] === 'single') {
      if (query === 'off') {
        $('.bleed').removeClass('singlePages')
      } else {
        $('.bleed').addClass('singlePages')
      }
    } else if (queries[i] === 'bleed') {
      if (query === 'off') {
        $('.bleed').addClass('hideBleed')
      } else {
        $('.bleed').removeClass('hideBleed')
      }
    } else if (queries[i] === 'grid') {
      if (query === 'off') {
        $('.grids').hide()
      } else {
        $('.grids').show()
      }
    } else if (queries[i] === 'zoom') {
      $('section').css({
        'transform': 'scale(' + query + ')',
        'width': windowW / query
      })
      $('.sizer').children('input').attr('value', query * 100);
    }
  }
}

function zoomShortcut (sizer, windowW, direction) {
  var value = parseInt(sizer.val());
  var newVal;
  if (direction === 'plus') {
    newVal = value + 5;
  } else {
    newVal = value - 5;
  }
  sizer.val(newVal);
  var el = 'zoom';
  $('section').css({
    'transform': 'scale(' + (newVal / 100) + ')',
    'width': windowW / (newVal / 100)
  })
  changeQuery (el, value / 100)
}

function imagesPrint (expos, imgs) {
  if ($('body').hasClass('print')) {
    imgs.each(function() {
      var linksHD = $(this).parent('a').attr('href');
        $(this).attr('src', linksHD);
    })
  }
}

function showPageId(part){
  part.mouseenter(function (){
    var id = $(this).parents('section').attr('id');
    var idNumber = id.substr(4);
    $('.pageId.' + idNumber).show();
  }).mouseleave(function () {
    $('.pageId').hide();
  })
}
