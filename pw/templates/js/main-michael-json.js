
$(window).on('load', function(){
  var h1Loc = $('#page1026 .center').find(' > h1');

  var comptIllu = 1;
  h1Loc.each(function(){
    $('<div/>',{
      class: 'illu',
      id: 'h1-illu'+comptIllu,
    }).insertBefore(this);
    comptIllu++;
  })

  MichaelJson({
    data: 'fimpah5',
    locate: '#h1-illu1',
    elementsClass: 'cellule4',
    font: 'herbier',
    glyphs: ['7','j', 'h', '1', '2'],
    findColor: '#fefffc',
    glyphColor: ['#15B965','#ff334d'],
    freq: [1,2],
    rotate: 0,
    translateX: 100,
    incTransX: 0.1,
    translateY: 50,
    incTransY: 0,
    scan: ['all'],
    incStyle: [
      ['font-size', 30, 'px', -0.05],
    ]
  });
  MichaelJson({
    data: 'fimpah5',
    locate: '#h1-illu1',
    elementsClass: 'cellule',
    font: 'herbier',
    glyphs: ['p', 'i'],
    findColor: '#ff0000',
    glyphColor: ['#15B965','blue'],
    freq: [1,3],
    rotate: 0,
    translateX: 100,
    incTransX: 0,
    translateY: 50,
    incTransY: 0,
    scan: ['all'],
    incStyle: [
      ['font-size', 40, 'px', 0],
    ]
  });
})

var first = true;

$(window).on('scroll', function(){

  if (first == true) {

    var h2Loc = $('#page1026 .center').find(' > h2');
    var h3Loc = $('#page1026 .center').find(' > h3');

    var glyphz = ['1', ' ', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'f', 'h', 'i', 'j', 'm', 'n', 'o', 'p', 'q', 'r'];
    var colorz = ['#4D20E8', '#ff334d', '#15B965']



    var comptIllu = 1;
    h2Loc.each(function(){
      $('<div/>',{
    	  class: 'illu illu2',
    	  id: 'h2-illu'+comptIllu,
      }).insertBefore(this);

      var randGlyph1 = glyphz[Math.floor(Math.random()*glyphz.length)];
      var randGlyph2 = glyphz[Math.floor(Math.random()*glyphz.length)];
      // var randGlyph3 = glyphz[Math.floor(Math.random()*glyphz.length)];
      var randColor1 = colorz[Math.floor(Math.random()*colorz.length)];
      var randColor2 = colorz[Math.floor(Math.random()*colorz.length)];

      MichaelJson({
        data: 'fimpah2',
        locate: '#h2-illu'+comptIllu,
        elementsClass: 'cellule',
        font: 'herbier',
        glyphs: [randGlyph1, randGlyph2],
        findColor: '#000000',
        glyphColor: [randColor1, randColor2],
        freq: [1,1],
        rotate: 0,
        translateX: 75,
        incTransX: 2,
        translateY: 25,
        incTransY: 0,
        scan: ['all'], //
        incStyle: [
          ['font-size', 42, 'px', 0],
        ]
      });
    	comptIllu++;
    })

    var comptIllu = 1;
    h3Loc.each(function(){
      $('<div/>',{
    	  class: 'illu illu2',
    	  id: 'h3-illu'+comptIllu,
      }).insertBefore(this);

      var randGlyph1 = glyphz[Math.floor(Math.random()*glyphz.length)];
      var randGlyph2 = glyphz[Math.floor(Math.random()*glyphz.length)];
      // var randGlyph3 = glyphz[Math.floor(Math.random()*glyphz.length)];
      var randColor1 = colorz[Math.floor(Math.random()*colorz.length)];
      var randColor2 = colorz[Math.floor(Math.random()*colorz.length)];

      MichaelJson({
        data: 'fimpah2',
        locate: '#h3-illu'+comptIllu,
        elementsClass: 'cellule',
        font: 'herbier',
        glyphs: [randGlyph1, randGlyph2],
        findColor: '#000000',
        glyphColor: [randColor1, randColor2],
        freq: [1,1],
        rotate: 0,
        translateX: 75,
        incTransX: 2,
        translateY: 25,
        incTransY: 0,
        scan: ['all'], //
        incStyle: [
          ['font-size', 42, 'px', 0],
        ]
      });
    	comptIllu++;
    })

    first = false;
  }
  //

});
