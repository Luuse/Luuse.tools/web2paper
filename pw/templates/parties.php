<?php
  include("./inc/head.php");
?>
<?php foreach ($page->children as $key => $partie) {
  $id = $partie->id;
  $template = $partie->template;
  ?>
  <div class="pageId <?= $id ?>">
    #part<?= $id ?> (template: <?= $template ?>)
  </div>
  <section class="part part<?= $key ?> <?= $template ?>" id="part<?= $id ?>">


  <?php include('./get_content.php'); ?>
  </section>
  <?php
}

include("./inc/foot.php");

?>
