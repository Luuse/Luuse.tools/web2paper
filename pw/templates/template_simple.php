<div class="singleContent singleContent<?= $key ?> <?= $partie->id ?>">
  <div class="content content0" id="content<?= $partie->id ?>">
    
    <h1><?= $titre ?></h1>
    <div class="textSimple textFr">
      <?= $texteFr ?>
    </div>
    <div class="textSimple textEn">
      <?= $texteEn ?>
    </div>

  </div>
  <div class="content1">

    <?php foreach ($images as $key => $image): ?>
      <img src="<?= $image->url ?>" alt="">
    <?php endforeach; ?>

  </div>
</div>
