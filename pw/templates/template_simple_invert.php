<div class="singleContent singleContent<?= $key ?> <?= $partie->id ?>">
  <div class="content content0" id="content<?= $partie->id ?>">

    <?php foreach ($images as $key => $image): ?>
      <img src="<?= $image->url ?>" alt="">
    <?php endforeach; ?>
  </div>
  <div class="content1">
    <h1><?= $titre ?></h1>
    <div class="textSimple textFr">
      <?= $texteFr ?>
    </div>
    <div class="textSimple textEn">
      <?= $texteEn ?>
    </div>
  </div>
</div>
